#!/usr/bin/python3

from collections import defaultdict
import urllib.request
import datetime
import sys
import json
import os
import argparse
import shutil
import re

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dir", default="./public",
                    help="select the output directory")
args = parser.parse_args()

languages = {'fr': 'French', 'it': 'Italian', 'de': 'German', 'da': 'Danish', 'ja': 'Japanese', 'sk': 'Slovak', 'ru': 'Russian', 'pt_BR': 'Brazilian Portuguese', 'ko': 'Korean', 'pl': 'Polish'}

def setcolor(number, total):
    ratio = number/total
    red = 51+int(170 * ratio)
    green = 221-int(170 * ratio)
    return '#%s%s33' % (hex(red).lstrip('0x'), hex(green).lstrip('0x'))

def getNumbers(lang):
    # Read the allpackages page
    url = "https://ddtp.debian.org/ddt.cgi?allpackages="+lang
    request = urllib.request.urlopen(url)
    page = request.read().decode('utf-8')
    now = datetime.datetime.now(datetime.timezone.utc)

    all_packages = defaultdict(list)
    untrans_packages = defaultdict(list)
    color = dict()
    soup_all_packs = re.findall("<a href=\"ddt\.cgi.*>(.*)<\/a>(.*)", page, re.M)
    key = ""

    # Triage packages
    # For each package, soup_all_packs contains two elements: the package name and its status
    for n in range (len(soup_all_packs)):
      # first determine the key
      name = soup_all_packs[n][0]
      if name[0].isdigit():
        key = "0-9"
      elif (name[:3] == "lib"):
        if name[3].isdigit():
          key = "lib0-9"
        else:
          key = name[:4]
      else:
        key = name[0]
      # add the package name to all_packages
      all_packages[key].append(name)
      # add the package to untrans_packages if not translated
      if (soup_all_packs[n][1] == "  untranslated"):
        untrans_packages[key].append(name)

    for k, v in sorted(all_packages.items()):
      color[k] = setcolor(len(untrans_packages[k]), len(v))

    nb_untrans = 0
    nb_all = 0
    dict_len_untrans = dict()
    dict_len_all = dict()
    for k, v in sorted(all_packages.items()):
      nb_untrans += len(untrans_packages[k])
      dict_len_untrans[k] = len(untrans_packages[k])
      nb_all += len(v)
      dict_len_all[k] = len(v)

    html_output = ""
    html_output += """<html>
    <head>
      <title>Untranslated package descriptions by letter</title>
      <link rel="stylesheet" href="../style.css">
    </head>
    <body><div>
    <h1>Untranslated package descriptions for """ + languages[lang] + """: """ + str(nb_untrans) + """ / """ + str(nb_all) + """</h1>
    <table>
    <tr>"""
    counter = 0
    for k, v in sorted(all_packages.items()):
      if (counter != 0) and (counter % 9 == 0):
        html_output += """</tr>
      <tr>
      """
      counter += 1
      html_output += """  <td style='background:"""+color[k]+"""'><div class='gradient'><span class='letter'>""" + k + """</span> <span class='number'>""" + str(len(untrans_packages[k])) + """</span><span class='total'>/""" + str(len(v)) + """</span></div></td>
      """
    html_output += """</tr>
    </table>
    </div><div>
    <p>Last updated on """ + now.strftime('%a %d %b %Y, %H:%M %Z') + """.</p>
    </div></body>
    </html>"""

    outputfile = args.dir + "/" + lang + "/packagesnumbersbyletter.html"
    os.makedirs(args.dir + "/" + lang, exist_ok=True)
    with open (outputfile, "w") as f:
        f.write(html_output)
    #print (html_output, file=output)

    current_stats = {'date': now.isoformat(), 'untranslated': dict_len_untrans, 'all': dict_len_all}

    try:
        json_file = open (args.dir + "/" + lang + "/stats.json")
        global_dict = json.load(json_file)
    except IOError:
        global_dict = {'language': lang, 'stats': []}
    global_dict['stats'].append(current_stats)
    json_output = open (args.dir + "/" + lang + "/stats.json", "w")
    json.dump (global_dict, json_output, sort_keys=True)

os.makedirs(args.dir, exist_ok=True)
shutil.copy('style.css', args.dir)

def createIndexPage():
  html_output = """<html>
  <head>
    <title>Some DDTSS numbers</title>
  </head>
  <body>
    <div>DDTSS provides <a href="https://ddtp2.debian.net/stats/stats-sid.html">some statistics</a> for each language. This page proposes additional statistics for the following languages:</div>
    <ul>
  """
  for k in languages:
    html_output += "<li><a href=\"" + k + "/packagesnumbersbyletter.html\">" + languages[k] + "</a> (<a href=\"" + k + "/stats.json\">JSON</a>)</li>"
  html_output += """
    </ul>
  </body></html>
  """
  outputfile = args.dir + "/index.html"
  with open (outputfile, "w") as f:
    f.write(html_output)

for k in languages:
  getNumbers(k)
createIndexPage()
